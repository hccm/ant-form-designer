import axios from 'axios'
import notification from 'ant-design-vue/lib/notification'

// 使用create方法创建axios实例
export const service = axios.create({
  timeout: 30000, // 请求超时时间
  baseURL: '',
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
})
// 添加请求拦截器
service.interceptors.request.use(config => {
  // Object.assign(config.headers, headers())
  return config
})
// 添加响应拦截器
service.interceptors.response.use(response => {
  const data = response.data || {}
  if (data.code === -10) {
    notification.warning({
      message: '警告',
      description: '请勿在2秒内多次触发同一接口！'
    })
  }
  return data
}, error => {
  console.log('requestError:', error)
  const msg = error.Message !== undefined ? error.Message : '请求出错'
  notification.error({
    message: msg,
    description: ''
  })
  return Promise.reject(error)
})
